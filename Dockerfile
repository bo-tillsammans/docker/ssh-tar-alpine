FROM alpine:edge
MAINTAINER Johan Wänglöf <jwanglof@gmail.com>

# add openssh and clean
RUN apk add --no-cache --update openssh \
    && rm  -rf /tmp/*

CMD ["/bin/sh"]